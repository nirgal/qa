## Match: regexp:dpkg-shlibdeps: warning:\/ .* should't\/ be linked with .* (it\/ uses non of its symbols)
## Match: regexp:dpkg-shlibdeps: warning:\/ dependency\/ on [a-z_0-9+-]*.* could \/be avoided if .* were \/not uselessly linked
## Since: 1
<p>
The build logs contains a like like
</p>
<pre>
dpkg-shlibdeps: warning: dependency on LIBRARY could be avoided if "BINARY" were not uselessly linked...
</pre>
<p>
This means a binary links to a library but dpkg-shlibs found no symbols used from it.
Possible reasons:
</p>
<ul>
<li>The library is linked in to have one of it's initializers run.
<br>
Removing the library might silently change the behaviour of the program.
<li>The build system links in all libraries to all its binaries though they
are only needed in some of them.
<br>
This slows down starting those programs a little bit. Unless some packages'
dependencies change due to this, it's harmless.
<li>The program uses only functions of a library that are inlined.
<br>
(Note that some functions in <tt>math.h</tt> might be inlined for some architectures
 but not for others, causing warning about <tt>-lm</tt> on some while it cannot be
 removed on others.)
<li>A library is linked against but never used.
<br>
Just remove the linking, if you are sure it is this case and not one of the above.
<li>The build system links in all libraries needed when static linking.
even if using shared libraries. (As static libraries have no dependency
information, a program linking it needs to also link in all the dependencies).
<br>
This introduces the (theoretical) risk that when the depedency-only lib is changing
sonames, this might cause both libraries to be pulled in.
<br>
This can be fixed by removing those libraries, after making sure it is really this
case and not one of the previous ones.
<li>The program uses a set of libraries that ignore library boundaries (for example many libglib using libraries).
<br>
As headers might inline functions with calls to other libraries and this might change
between versions, removing the <tt>-l</tt> is not possible.
Here <tt>-Wl,--as-needed</tt> is the only solution.
</ul>
