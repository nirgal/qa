#! /usr/bin/env python3
#  Copyright (C) 2002 Igor Genibel
#  Copyright (C) 2006 Christoph Berg <myon@debian.org>
#  Copyright (C) 2018 Paul Gevers <elbrus@debian.org>
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.

import sys, os, re, dbm, ssl
from urllib.request import urlopen
import yaml

def extract_excuses():
    excuses_db = dbm.open("excuses-new", 'c')
    debian_ca_path = '/etc/ssl/ca-debian'
    if os.path.isdir(debian_ca_path):
        context = ssl.create_default_context(capath=debian_ca_path)
    else:
        context = None

    try:
        with urlopen("https://release.debian.org/britney/excuses.yaml", timeout=10, context=context) as stream:
            ex = yaml.safe_load(stream)
    except OSError as err:
        print('WARNING: skipping testing-excuses due to: %s!' % err.reason, file=sys.stderr)
        return

    re_less_eq     = re.compile(r'\(<= ')
    re_less        = re.compile(r'\(<< ')
    re_less_old    = re.compile(r'\(< ')
    re_greater_eq  = re.compile(r'\(>= ')
    re_greater     = re.compile(r'\(>> ')
    re_greater_old = re.compile(r'\(> ')
    re_target = re.compile(r' target="_blank"')

    for source in ex['sources']:
        package = source['source']
        # Removals, (testing-)proposed-updates and binary only
        if source['item-name'] != package:
            continue

        version = source['old-version']
        excuse = "<!-- Testing_version: " + version + " -->\n"
        excuse += "<h2>Excuse for " + package + "</h2>\n<ul>"

        for line in source['excuses']:
            line = re_less.sub('(&lt;&lt; ', line)
            line = re_less_eq.sub('(&lt;= ', line)
            line = re_less_old.sub('(&lt; ', line)
            line = re_greater.sub('(&gt;&gt; ', line)
            line = re_greater_eq.sub('(&gt;= ', line)
            line = re_greater_old.sub('(&gt; ', line)
            line = re_target.sub('', line)
            excuse += '<li>' + line + '</li>\n'

        try:
            for depends in source['dependencies']['blocked-by']:
                excuse += '<li>Depends: ' + package
                excuse += ' <a href="excuses.php?package='
                excuse +=  depends + '">' + depends
                excuse += '</a> (not considered)</li>\n'
        except KeyError:
            pass

        try:
            for depends in source['dependencies']['migrate-after']:
                if '/' not in depends:
                    excuse += '<li>Depends: ' + package
                    excuse += ' <a href="excuses.php?package='
                    excuse +=  depends + '">' + depends
                    excuse += '</a></li>\n'
        except KeyError:
            pass

        if source['is-candidate'] == 'false':
            excuse += '<li>Not considered</li>\n'
        excuse += '</ul>'

        excuses_db[package] = excuse

    excuses_db.close()
    return

extract_excuses()

# vim:sw=4:softtabstop=4:expandtab
