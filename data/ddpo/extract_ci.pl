#!/usr/bin/perl

use strict;
use warnings;
use JSON;
use DB_File;

my $suite = shift or die "Syntax: $0 suite packages.json";

undef $/; # slurp mode
my $js = <>;
my $ci = decode_json ($js);

my $db_filename = 'ci-new.db';
my %db;
my $db_file = tie %db, "DB_File", $db_filename, O_RDWR|O_CREAT, 0666, $DB_BTREE
    or die "Can't open database $db_filename : $!";

# [
#   {
#     'message' => 'Tests passed',
#     'date' => '2014-01-28',
#     'status' => 'pass',
#     'duration_human' => '0h 0m 5s',
#     'duration_seconds' => '5',
#     'version' => '0.0.15-2',
#     'package' => '3depict'
#   },

foreach my $pkg (@$ci) {
	$db{"status:$suite:$pkg->{package}"} = $pkg->{status};
	$db{"version:$suite:$pkg->{package}"} = $pkg->{version};
	$db{"title:$suite:$pkg->{package}"} = "$pkg->{message} for $pkg->{version} ($pkg->{date}, took $pkg->{duration_human})";
}
