#use wml::templ::template title="Man pages" author="Martin Michlmayr"

<P>As per Debian's Policy, every executable file in Debian has to
supply a man page.  However, many packages don't follow this
requirement yet.  Below is a listing of all executables for which no
corresponding man page is available yet.  If you use any of these
packages listed below, please consider writing a man page for its
executable files and submit it to the
<A HREF="https://bugs.debian.org/">Bug Tracking System</A> (BTS).

<P>Writing man pages is not very difficult.  You can simply look at
some man pages in /usr/share/man and follow their structure (also see the
file /usr/share/doc/man-db/examples/manpage.example).  Furthermore, some
information about the format of man pages is available
in the
<A HREF="http://gmanedit.sourceforge.net/man7.html">man</A>
(man 7 man) and
<A HREF="https://search.cpan.org/~jhi/perl-5.8.1/pod/pod2man.PL">pod2man</A>
(man 1 pod2man) man pages.  You can also install any of various packages
which provide tools to help you write basic man pages, and then tweak
the output if necessary: help2man, txt2man, xml2man (in the kdelibs-bin
package), or docbook-to-man.

<P>When you decide to help out, please write the man pages for a whole
package.

<P>When you have written a man page, please submit the man page to the BTS.
Before submitting a new bug, please check the bug listing (linked from
the package name) for an existing bug.  If a bug exists already,
send your man page to <I>nnn</I>@bugs.debian.org where <I>nnn</I> is
the bug number (e.g. 92423@bugs.debian.org).  If there is no bug yet,
file a new one by sending a message to submit@bugs.debian.org.

<P>If you have written a man page for the executable file <I>foobar</I>
in the package <I>bar</I>, you would send a bug report like this:

<PRE>
To: &lt;nnn or submit&gt;@bugs.debian.org
Subject: man page for foobar

Package: bar
Tags: patch

&lt;attach foobar.1 here&gt;
</PRE>

<P>Also, note that you can have a list of the programs installed in your
Debian system that miss a manpage using the command manpage-alert
from package devscripts. It is usually easier to write documentation for
programs that you use frequently, so that list could be more helpful
than the one here under to find a manpage to work on.

<protect pass=4->

<:

use strict;
use POSIX qw(strftime);
use HTML::Entities;

sub protect {
    encode_entities($_[0], "\"&'<>");
}

my $missing = 0;
open(LINTIAN, "/srv/qa.debian.org/data/lintian.log") || die "Cannot open lintian report";
my $lintian_date = (stat LINTIAN)[9];
my %man;
while(<LINTIAN>) {
    my (undef, $package, $error) = split(/: /);
    $package =~ s/ .*//;

    if ($error =~ /^(link-to-undocumented-manpage|binary-without-manpage)/) {
        my (undef, $file) = split(/ /, $error);
        push(@{$man{$package}}, $file);
        $missing++;
    }
}
close(LINTIAN);

print "<P>In total $missing man pages in ";
print scalar keys %man, " packages are missing at the moment. ";
print "This listing was generated from a Lintian report published on ";
print strftime '%a, %d %b %Y', gmtime $lintian_date;
print ".</P>\n";

foreach my $package (sort keys %man) {
    print "<B><A HREF=\"https://bugs.debian.org/", protect($package), "\">",
        protect($package), "</A></B>: ";
    print join ", ", map {
        chomp;
        if (s/\.gz$//) {
            s#.*/##;
        }
        protect($_);
    } (@{$man{$package}});
    print " <BR>\n";
}

:>

</protect>
