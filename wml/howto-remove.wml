#use wml::templ::template title="How to go about QA removals" author="Jeroen van Wolffelaar"

<H2>Judging orphaned packages for possible removal</H2>

<P>A lot of packages are orphaned, sometimes for a long time. If nobody has
any interest in them, they might be better off being removed from Debian.

<p> This document aims to list criteria to judge whether a package should
be removed. Below you will find how to actually file a removal request.

<p><a href="developer.php?login=packages@qa.debian.org">Here</a> and <a
href="orphaned.html">here</a> you can find lists of orphaned packages.

<UL>
  <LI>How long has the package been orphaned? If it was only recently
  orphaned, maybe someone is interested and will want to pick it up. You
  should mostly give potential adopters some time to find the package, unless
  it's very obvious the package is of no great use.
  <LI>Does the package have release-critical bugs? Or any important bug at
  all?
  <li>Is the package (still) useful? Does it still work?
  <li>Does anything depend on it?
  <li>Was it ever part of a stable release?
  <li>Did the release team already remove the package from testing?
  <li>Does the package have an added value to the archive, does it perform
  some function that no other package can do?
  <li>Is upstream still active?
  <li>If the package is outdated with respect to upstream, did anyone care
  about it and file a wishlist bug? Related: is the package in use by anyone?
  See <a href="https://popcon.debian.org/">popcon</a> statistics for that, or
  the amount of bugreports.
</UL>

Please see <a
href="https://lists.debian.org/debian-qa/2004/09/msg00049.html">this mailing
list post</a> for a suggested way to title removal requests. Usually, you
should retitle the wnpp bug and reassign it to the <tt>ftp.debian.org</tt> <a
href="https://www.debian.org/Bugs/pseudo-packages">pseudo-package</a>.

<p>Sometimes, for example if a package is very buggy, it makes sense to
clone the removal bug as a RC bug for the package, and ask the
release team to remove it from testing. This isn't often needed, because
packages get removed from testing automatically after being dropped from
unstable, though it would be applicable when Debian is close to a release.

<p>If you have any questions or doubts, please mail the <a
href="https://lists.debian.org/debian-qa/">debian-qa mailinglist</a>. If
you're just unsure about whether to remove a package, you can always just mail
the wnpp bug with your thoughts, someone else will (eventually) process it
then.
